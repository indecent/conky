--[[
Clock Rings by londonali1010 (2009)

This script draws percentage meters as rings, and also draws clock hands if you want! It is fully customisable; all options are described in the script. This script is based off a combination of my clock.lua script and my rings.lua script.

IMPORTANT: if you are using the 'cpu' function, it will cause a segmentation fault if it tries to draw a ring straight away. The if statement near the end of the script uses a delay to make sure that this doesn't happen. It calculates the length of the delay by the number of updates since Conky started. Generally, a value of 5s is long enough, so if you update Conky every 1s, use update_num > 5 in that if statement (the default). If you only update Conky every 2s, you should change it to update_num > 3; conversely if you update Conky every 0.5s, you should use update_num > 10. ALSO, if you change your Conky, is it best to use "killall conky; conky" to update it, otherwise the update_num will not be reset and you will get an error.

To call this script in Conky, use the following (assuming that you save this script to ~/scripts/rings.lua):
	lua_load ~/scripts/clock_rings-v1.1.1.lua
	lua_draw_hook_pre clock_rings

Changelog:
+ v1.1.1 -- Fixed minor bug that caused the script to crash if conky_parse() returns a nil value (20.10.2009)
+ v1.1 -- Added colour option for clock hands (07.10.2009)
+ v1.0 -- Original release (30.09.2009)
]]

require 'cairo'

cur_dir = os.getenv("HOME")
conf_dir = cur_dir .. '/.conky/lua'
dofile(conf_dir .. '/settings.lua')
dofile(cur_dir .. '/.conky/locals.lua')
local_settings = local_conf()

-- Use these settings to define the origin and extent of your clock.

clock_r=local_settings.clock_r

-- "mem_x" and "mem_y" are the coordinates of the centre of the clock, in pixels, from the top left of the Conky window.

mem_x=local_settings.mem_x_loc
mem_y=local_settings.mem_y_loc
cpu_x=local_settings.cpu_x_loc
cpu_y=local_settings.cpu_y_loc
clock_x=local_settings.clock_x_loc
clock_y=local_settings.clock_y_loc
cpu_table={}
upspeed_table={}
downspeed_table={}

-- Colour & alpha of the clock hands

clock_colour=0xffaa11
clock_alpha=0.4

function draw_hex_from_top(cr,x_loc,y_loc,--[[optional]]inner_text)
  --[[ Constants ]]
	local font="Santana"
	local font_size=15
  local line_width=1
  local line_cap=CAIRO_LINE_CAP_BUTT
  local red,green,blue,alpha=1,1,1,1
	local font_slant=CAIRO_FONT_SLANT_NORMAL
	local font_face=CAIRO_FONT_WEIGHT_NORMAL

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  
  cairo_move_to(cr,x_loc,y_loc)
  cairo_line_to (cr,x_loc-40,y_loc+25)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc-40,y_loc+25)
  cairo_line_to (cr,x_loc-40,y_loc+70)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc-40,y_loc+70)
  cairo_line_to (cr,x_loc,y_loc+95)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc,y_loc)
  cairo_line_to (cr,x_loc+40,y_loc+25)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc+40,y_loc+25)
  cairo_line_to (cr,x_loc+40,y_loc+70)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc+40,y_loc+70)
  cairo_line_to (cr,x_loc,y_loc+95)
  cairo_stroke (cr)

	cairo_select_font_face (cr, font, font_slant, font_face)
  cairo_set_font_size (cr, font_size)
  if inner_text then
    if string.len(inner_text) == 1 then
      cairo_line_to (cr,x_loc,y_loc+55)
    elseif string.len(inner_text) < 3 then
      cairo_line_to (cr,x_loc-10,y_loc+55)
    elseif string.len(inner_text) == 5 then
      cairo_line_to (cr,x_loc-24,y_loc+55)
    else
      cairo_line_to (cr,x_loc-14,y_loc+55)
    end
  end
  cairo_show_text(cr,inner_text)
	cairo_stroke (cr)
end

function draw_hex_from_left_side(cr,x_loc,y_loc,--[[optional]]inner_text)
  --[[ Constants ]]
	local font="Santana"
	local font_size=15
  local line_width=1
  local line_cap=CAIRO_LINE_CAP_BUTT
  local red,green,blue,alpha=1,1,1,1
	local font_slant=CAIRO_FONT_SLANT_NORMAL
	local font_face=CAIRO_FONT_WEIGHT_NORMAL

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  
  cairo_move_to(cr,x_loc,y_loc-23)
  cairo_line_to (cr,x_loc,y_loc+22)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc,y_loc-23)
  cairo_line_to (cr,x_loc+40,y_loc-47)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc,y_loc+22)
  cairo_line_to (cr,x_loc+40,y_loc+47)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc+40,y_loc-47)
  cairo_line_to (cr,x_loc+80,y_loc-23)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc+80,y_loc-23)
  cairo_line_to (cr,x_loc+80,y_loc+22)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc+80,y_loc+22)
  cairo_line_to (cr,x_loc+40,y_loc+47)
  cairo_stroke (cr)

	cairo_select_font_face (cr, font, font_slant, font_face)
  cairo_set_font_size (cr, font_size)
  if inner_text then
    if inner_text == 'UP/DOWN' then
      cairo_line_to (cr,x_loc+2,y_loc+5)
    elseif string.len(inner_text) == 1 then
      cairo_line_to (cr,x_loc+34,y_loc+5)
    elseif string.len(inner_text) < 3 then
      cairo_line_to (cr,x_loc+30,y_loc+5)
    elseif string.len(inner_text) == 3 then
      cairo_line_to (cr,x_loc+24,y_loc+5)
    elseif string.len(inner_text) == 4 then
      cairo_line_to (cr,x_loc+38,y_loc+5)
    elseif string.len(inner_text) == 5 then
      cairo_line_to (cr,x_loc+25,y_loc+5)
    elseif string.len(inner_text) == 6 then
      cairo_line_to (cr,x_loc+18,y_loc+5)
    elseif string.len(inner_text) == 7 then
      cairo_line_to (cr,x_loc+10,y_loc+5)
    else
      cairo_line_to (cr,x_loc+10,y_loc+5)
    end
  end
  cairo_show_text(cr,inner_text)
	cairo_stroke (cr)
end

function draw_hex_from_top_left(cr,x_loc,y_loc,--[[optional]]inner_text)
  --[[ Constants ]]
	local font="Santana"
	local font_size=15
  local line_width=1
  local line_cap=CAIRO_LINE_CAP_BUTT
  local red,green,blue,alpha=1,1,1,1
	local font_slant=CAIRO_FONT_SLANT_NORMAL
	local font_face=CAIRO_FONT_WEIGHT_NORMAL

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  
  cairo_move_to(cr,x_loc,y_loc)
  cairo_line_to (cr,x_loc+40,y_loc-25)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc,y_loc)
  cairo_line_to (cr,x_loc,y_loc+45)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc,y_loc+45)
  cairo_line_to (cr,x_loc+40,y_loc+70)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc+40,y_loc-25)
  cairo_line_to (cr,x_loc+80,y_loc)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc+80,y_loc)
  cairo_line_to (cr,x_loc+80,y_loc+45)
  cairo_stroke (cr)
  
  cairo_move_to(cr,x_loc+80,y_loc+45)
  cairo_line_to (cr,x_loc+40,y_loc+70)
  cairo_stroke (cr)

	cairo_select_font_face (cr, font, font_slant, font_face)
  cairo_set_font_size (cr, font_size)
  if inner_text then
    if string.len(inner_text) < 3 then
      cairo_line_to (cr,x_loc+30,y_loc+28)
    elseif string.len(inner_text) == 3 then
      cairo_line_to (cr,x_loc+25,y_loc+28)
    elseif string.len(inner_text) == 4 then
      cairo_line_to (cr,x_loc+30,y_loc+28)
    elseif string.len(inner_text) == 5 then
      cairo_line_to (cr,x_loc+25,y_loc+28)
    elseif string.len(inner_text) == 6 then
      cairo_line_to (cr,x_loc+20,y_loc+28)
    else
      cairo_line_to (cr,x_loc+10,y_loc+28)
    end
  end
  cairo_show_text(cr,inner_text)
	cairo_stroke (cr)
end

function conky_draw_lines(cr)
  --[[ Constants ]]
  local line_width=1
  local line_cap=CAIRO_LINE_CAP_BUTT
  local red,green,blue,alpha=1,1,1,1
	local font="Santana"
	local font_size=15
	local font_slant=CAIRO_FONT_SLANT_NORMAL
	local font_face=CAIRO_FONT_WEIGHT_NORMAL

  --[[ HR for clock]]
  local start_x=local_settings.clock_cpu_inner_arc_x_loc+90
  local start_y=local_settings.clock_y_loc+17
  local end_x=local_settings.clock_x_loc+800
  local end_y=local_settings.clock_y_loc+17

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_left_side(cr,end_x,end_y,'FS')

  --[[ Lines to disks ]]
  start_x = end_x+80
  start_y = end_y-23
  end_x = start_x+80
  end_y = start_y-50

  local_settings.fs_x_loc = end_x+local_settings.fs_radius+(local_settings.fs_thickness/2)
  local_settings.fs_y_loc = end_y-13

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  --[[ Draw a line and a hex ]]
  start_x = local_settings.fs_x_loc
  start_y = local_settings.fs_y_loc+local_settings.fs_radius+(local_settings.fs_thickness/2)+1
  end_x = start_x
  end_y = start_y+local_settings.fs_space-local_settings.fs_thickness-2

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_top(cr,end_x,end_y,'/')

  --[[ Draw line to next fs ring ]]
  start_x = local_settings.fs_x_loc+local_settings.fs_radius+(local_settings.fs_thickness/2)+1
  start_y = local_settings.fs_y_loc
  end_x = start_x+local_settings.fs_space-local_settings.fs_thickness-2
  end_y = start_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  --[[ Draw a line and a hex ]]
  start_x = end_x+local_settings.fs_radius+(local_settings.fs_thickness/2)+1
  start_y = end_y+local_settings.fs_radius+(local_settings.fs_thickness/2)+1
  end_x = start_x
  end_y = start_y+local_settings.fs_space-local_settings.fs_thickness-2

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_top(cr,end_x,end_y,'/home')
  
  --[[ Line to CPU ]]
  start_x=local_settings.clock_x_loc-40
  start_y=local_settings.clock_y_loc+80
  end_x=local_settings.clock_x_loc-95
  end_y=local_settings.clock_y_loc+190

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_top(cr,end_x,end_y,'CPU')

  start_x = end_x
  start_y = end_y+95
  end_x = start_x
  end_y = start_y+110

  local_settings.cpu_x_loc = end_x
  local_settings.cpu_y_loc = end_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  --[[ Line to CPU percent]]

  start_x = local_settings.cpu_x_loc+(local_settings.cpu_ring_count*(local_settings.cpu_ring_thickness+1))+local_settings.cpu_begin_radius-1
  start_y = local_settings.cpu_y_loc+(local_settings.cpu_ring_count*(local_settings.cpu_ring_thickness+1))+local_settings.cpu_begin_radius
  end_x = start_x+120
  end_y = start_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_left_side(cr,end_x,end_y,conky_parse('$cpu')..'%')

  --[[ GPU ]]

  start_x = end_x+80
  start_y = end_y
  end_x = start_x+120
  end_y = start_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_left_side(cr,end_x,end_y,'+')

  start_x = end_x+80
  start_y = end_y
  end_x = start_x+120
  end_y = start_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_left_side(cr,end_x,end_y,'GPU')

  start_x = end_x+80
  start_y = end_y
  end_x = start_x+105
  end_y = start_y

  local_settings.gpu_temp_x_loc = end_x+local_settings.gpu_radius+(local_settings.gpu_thickness/2)+1
  local_settings.gpu_temp_y_loc = end_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  start_x = local_settings.gpu_temp_x_loc+local_settings.gpu_radius+(local_settings.gpu_thickness/2)+1
  start_y = local_settings.gpu_temp_y_loc
  end_x = start_x+105
  end_y = start_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_left_side(cr,end_x,end_y,conky_parse('${hwmon 0 temp 1} °c'))

  --[[
  start_x = local_settings.gpu_temp_x_loc+local_settings.gpu_radius+(local_settings.gpu_thickness/2)+1
  start_y = local_settings.gpu_temp_y_loc
  end_x = start_x+120
  end_y = start_y

  local_settings.gpu_fan_x_loc = end_x+local_settings.gpu_radius+(local_settings.gpu_thickness/2)+1
  local_settings.gpu_fan_y_loc = end_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)
  ]]


  --[[ Line to network ]]

  start_x = local_settings.cpu_x_loc+30
  start_y = local_settings.cpu_y_loc+137
  end_x = start_x+75
  end_y = start_y+90

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_top_left(cr,end_x,end_y,conky_parse(string.format('${upspeed %s}',local_settings.interface)))

  --[[ Network HR ]]

  start_x = end_x+80
  start_y = end_y+23
  end_x = start_x+425
  end_y = start_y 

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_left_side(cr,end_x,end_y,'UP/DOWN')

  --[[ Line to memory ]]

  start_x = end_x+80
  start_y = end_y
  end_x = start_x+425
  end_y = start_y 

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)


  draw_hex_from_left_side(cr,end_x,end_y,conky_parse(string.format('${downspeed %s}',local_settings.interface)))

  --[[ Line to memory ]]

  start_x = end_x+80
  start_y = end_y
  end_x = start_x+150
  end_y = start_y

  local_settings.mem_x_loc = end_x+local_settings.mem_radius+(local_settings.mem_thickness/2)+1
  local_settings.mem_y_loc = end_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  start_x = local_settings.mem_x_loc+local_settings.mem_radius+(local_settings.mem_thickness/2)+1
  start_y = local_settings.mem_y_loc
  end_x = start_x+80
  end_y = start_y

  cairo_set_line_width(cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  cairo_move_to(cr,start_x,start_y)
  cairo_line_to (cr,end_x,end_y)
  cairo_stroke (cr)

  draw_hex_from_left_side(cr,end_x,end_y,conky_parse('$memperc'..'%'))


  --[[ Add text ]]
  start_x = local_settings.clock_x_loc
  start_y = local_settings.clock_y_loc

	cairo_select_font_face (cr, font, font_slant, font_face)
  cairo_set_font_size (cr, 30)
  cairo_line_to (cr,start_x-44,start_y+12)
  cairo_show_text(cr,conky_parse('${time %H:%M}'))
	cairo_stroke (cr)

	cairo_select_font_face (cr, font, font_slant, font_face)
  cairo_set_font_size (cr, 34)
  cairo_line_to (cr,start_x+120,start_y+60)
  cairo_show_text(cr,conky_parse('${time %A, %d %B %Y}'))
  cairo_stroke (cr)
end

function rgb_to_r_g_b(colour,alpha)
	return ((colour / 0x10000) % 0x100) / 255., ((colour / 0x100) % 0x100) / 255., (colour % 0x100) / 255., alpha
end

function draw_ring(cr,t,pt)
	local w,h=conky_window.width,conky_window.height

	local xc,yc,ring_r,ring_w,sa,ea=pt['x'],pt['y'],pt['radius'],pt['thickness'],pt['start_angle'],pt['end_angle']
	local bgc, bga, fgc, fga=pt['bg_colour'], pt['bg_alpha'], pt['fg_colour'], pt['fg_alpha']

	local angle_0=sa*(2*math.pi/360)-math.pi/2
	local angle_f=ea*(2*math.pi/360)-math.pi/2
	local t_arc=t*(angle_f-angle_0)

	-- Draw background ring

	cairo_arc(cr,xc,yc,ring_r,angle_0,angle_f)
	cairo_set_source_rgba(cr,rgb_to_r_g_b(bgc,bga))
	cairo_set_line_width(cr,ring_w)
	cairo_stroke(cr)

	-- Draw indicator ring

	cairo_arc(cr,xc,yc,ring_r,angle_0,angle_0+t_arc)
	cairo_set_source_rgba(cr,rgb_to_r_g_b(fgc,fga))
	cairo_stroke(cr)
end

function draw_hands(cr,xc,yc)
	local secs,mins,hours,secs_arc,mins_arc,hours_arc
	local xh,yh,xm,ym,xs,ys

  -- Do you want to show the seconds hand?
  
  local show_seconds=true

	secs=os.date("%S")
	mins=os.date("%M")
	hours=os.date("%I")

	secs_arc=(2*math.pi/60)*secs
	mins_arc=(2*math.pi/60)*mins+secs_arc/60
	hours_arc=(2*math.pi/12)*hours+mins_arc/12

	-- Draw hour hand

	xh=xc+0.7*clock_r*math.sin(hours_arc)
	yh=yc-0.7*clock_r*math.cos(hours_arc)
	cairo_move_to(cr,xc,yc)
	cairo_line_to(cr,xh,yh)

	cairo_set_line_cap(cr,CAIRO_LINE_CAP_ROUND)
	cairo_set_line_width(cr,5)
	cairo_set_source_rgba(cr,rgb_to_r_g_b(clock_colour,clock_alpha))
	cairo_stroke(cr)

	-- Draw minute hand

	xm=xc+clock_r*math.sin(mins_arc)
	ym=yc-clock_r*math.cos(mins_arc)
	cairo_move_to(cr,xc,yc)
	cairo_line_to(cr,xm,ym)

	cairo_set_line_width(cr,3)
	cairo_stroke(cr)

	-- Draw seconds hand

	if show_seconds then
		xs=xc+clock_r*math.sin(secs_arc)
		ys=yc-clock_r*math.cos(secs_arc)
		cairo_move_to(cr,xc,yc)
		cairo_line_to(cr,xs,ys)

		cairo_set_line_width(cr,1)
		cairo_stroke(cr)
	end
end

function conky_draw_rings()
	local function setup_rings(cr,pt)
      local str=''
      local value=0

      str=string.format('${%s %s}',pt['name'],pt['arg'])
      str=conky_parse(str)

      value=tonumber(str)
      if value == nil then value = 0 end
      pct=value/pt['max']

      conky_draw_lines(cr)
      draw_ring(cr,pct,pt)
    end



	-- Check that Conky has been running for at least 5s

	if conky_window==nil then return end
	local cs=cairo_xlib_surface_create(conky_window.display,conky_window.drawable,conky_window.visual, conky_window.width,conky_window.height)

	local cr=cairo_create(cs)	

	local updates=conky_parse('${updates}')
  update_num=tonumber(updates)
  
  local settings_table = ring_settings(local_settings)

	if update_num>5 then
		for i in pairs(settings_table) do
			setup_rings(cr,settings_table[i])
		end
	end

	draw_hands(cr,clock_x,clock_y)
end