function dummy_settings (local_conf)
  settings_table = {
    --[[{
      -- Edit this table to customise your rings.
      -- You can create more rings simply by adding more elements to settings_table.
      -- "name" is the type of stat to display; you can choose from 'cpu', 'memperc', 'fs_used_perc', 'battery_used_perc'.
      name='time',
      -- "arg" is the argument to the stat type, e.g. if in Conky you would write ${cpu cpu0}, 'cpu0' would be the argument. If you would not use an argument in the Conky variable, use ''.
      arg='%I.%M',
      -- "max" is the maximum value of the ring. If the Conky variable outputs a percentage, use 100.
      max=12,
      -- "bg_colour" is the colour of the base ring.
      bg_colour=0xffffff,
      -- "bg_alpha" is the alpha value of the base ring.
      bg_alpha=0,
      -- "fg_colour" is the colour of the indicator part of the ring.
      fg_colour=0xffffff,
      -- "fg_alpha" is the alpha value of the indicator part of the ring.
      fg_alpha=0,
      -- "x" and "y" are the x and y coordinates of the centre of the ring, relative to the top left corner of the Conky window.
      x=x_loc, y=y_loc,
      -- "radius" is the radius of the ring.
      radius=50,
      -- "thickness" is the thickness of the ring, centred around the radius.
      thickness=5,
      -- "start_angle" is the starting angle of the ring, in degrees, clockwise from top. Value can be either positive or negative.
      start_angle=0,
      -- "end_angle" is the ending angle of the ring, in degrees, clockwise from top. Value can be either positive or negative, but must be larger than start_angle.
      end_angle=360
    }-- ]]
  }
  return
end

function ring_settings (local_conf)
  num_cpu=local_conf.num_cpu
  sections=num_cpu/4
  section=1
  begin_angle=(((360/sections)/2))-((360/sections))
  begin_radius=local_conf.cpu_begin_radius
  cpu_ring_count=local_conf.cpu_ring_count
  cpu_ring_thickness=local_conf.cpu_ring_thickness
  cur_radius=begin_radius
  settings_table = {
    --[[ CPU ring ]]
    --[[
    {
      name='cpu',
      arg='cpu0',
      max=100,
      bg_colour=0xFFFFFF,
      bg_alpha=0.1,
      fg_colour=0xFFFFFF,
      fg_alpha=0.7,
      x=local_conf.fs_x_loc, y=local_conf.fs_y_loc,
      radius=143,
      thickness=14,
      start_angle=260,
      end_angle=320
    },
    {
      name='cpu',
      arg='',
      max=1,
      bg_colour=0xffffff,
      bg_alpha=0.15,
      fg_colour=0xffffff,
      fg_alpha=0,
      x=local_conf.cpu_x_loc, y=local_conf.cpu_y_loc,
      radius=95,
      thickness=14,
      start_angle=160,
      end_angle=200
    },
    {
      name='cpu',
      arg='',
      max=1,
      bg_colour=0xffffff,
      bg_alpha=0.15,
      fg_colour=0xffffff,
      fg_alpha=0,
      x=local_conf.cpu_x_loc, y=local_conf.cpu_y_loc,
      radius=95,
      thickness=14,
      start_angle=40,
      end_angle=80
    },
    {
      name='cpu',
      arg='',
      max=1,
      bg_colour=0xffffff,
      bg_alpha=0.15,
      fg_colour=0xffffff,
      fg_alpha=0,
      x=local_conf.cpu_x_loc, y=local_conf.cpu_y_loc,
      radius=95,
      thickness=14,
      start_angle=280,
      end_angle=320
    },
    ]]
    --[[ Time ring ]]
    {
      name='time',
      arg='%S',
      max=60,
      bg_colour=0xffffff,
      bg_alpha=0.1,
      fg_colour=0xffffff,
      fg_alpha=0.35,
      x=local_conf.clock_x_loc, y=local_conf.clock_y_loc,
      radius=70,
      thickness=local_conf.clock_sec_thickness,
      start_angle=0,
      end_angle=360
    },
    {
      name='time',
      arg='%M.%S',
      max=60,
      bg_colour=0xffffff,
      bg_alpha=0.1,
      fg_colour=0xffffff,
      fg_alpha=0.35,
      x=local_conf.clock_x_loc, y=local_conf.clock_y_loc,
      radius=76,
      thickness=local_conf.clock_min_thickness,
      start_angle=0,
      end_angle=360
    },
    {
      name='time',
      arg='%I.%M',
      max=12,
      bg_colour=0xffffff,
      bg_alpha=0.1,
      fg_colour=0xffffff,
      fg_alpha=0.35,
      x=local_conf.clock_x_loc, y=local_conf.clock_y_loc,
      radius=84,
      thickness=local_conf.clock_hour_thickness,
      start_angle=0,
      end_angle=360
    },
    {
      name='cpu', -- dummy (used for arc)
      arg='',
      max=1,
      bg_colour=0xd5dcde,
      bg_alpha=0.7,
      fg_colour=0xd5dcde,
      fg_alpha=0,
      x=local_conf.clock_cpu_inner_arc_x_loc, y=local_conf.clock_y_loc,
      radius=local_conf.clock_cpu_inner_arc_radius,
      thickness=2,
      start_angle=88,
      end_angle=112
    },
    {
      name='cpu', -- dummy (used for arc)
      arg='',
      max=1,
      bg_colour=0xffffff,
      bg_alpha=0.7,
      fg_colour=0xffffff,
      fg_alpha=0,
      x=local_conf.clock_cpu_outer_arc_x_loc, y=local_conf.clock_y_loc,
      radius=local_conf.clock_cpu_outer_arc_radius,
      thickness=2,
      start_angle=89,
      end_angle=104
    },
    --[[ Memory ring settings ]]
    {
      name='memperc',
      arg='',
      max=100,
      bg_colour=0xffffff,
      bg_alpha=0.1,
      fg_colour=0xffffff,
      fg_alpha=0.7,
      x=local_conf.mem_x_loc, y=local_conf.mem_y_loc,
      radius=local_conf.mem_radius,
      thickness=local_conf.mem_thickness,
      start_angle=-180,
      end_angle=180
    },
    --[[ Filesystem ]]
    {
      name='fs_used_perc',
      arg='/',
      max=100,
      bg_colour=0xFFFFFF,
      bg_alpha=0.1,
      fg_colour=0xFFFFFF,
      fg_alpha=0.7,
      x=local_conf.fs_x_loc, y=local_conf.fs_y_loc,
      radius=local_conf.fs_radius,
      thickness=local_conf.fs_thickness,
      start_angle=-180,
      end_angle=180
    },
    {
      name='fs_used_perc',
      arg='/home',
      max=100,
      bg_colour=0xFFFFFF,
      bg_alpha=0.1,
      fg_colour=0xFFFFFF,
      fg_alpha=0.7,
      x=local_conf.fs_x_loc+(local_conf.fs_radius*2)+local_conf.fs_space, y=local_conf.fs_y_loc,
      radius=local_conf.fs_radius,
      thickness=local_conf.fs_thickness,
      start_angle=-180,
      end_angle=180
    },
    --[[ GPU ]]
    --[[
    {
      -- GPU fan
      name='hwmon 0 fan 1',
      arg='',
      max=5000,
      bg_colour=0xFFFFFF,
      bg_alpha=0.4,
      fg_colour=0xFFFFFF,
      fg_alpha=0.8,
      x=local_conf.gpu_fan_x_loc, y=local_conf.gpu_fan_y_loc,
      radius=local_conf.gpu_radius,
      thickness=local_conf.gpu_thickness,
      start_angle=-180,
      end_angle=180
    },
    ]]
    {
      -- GPU temp
      name='hwmon 0 temp 1',
      arg='',
      max=85,
      bg_colour=0xFFFFFF,
      bg_alpha=0.4,
      fg_colour=0xFFFFFF,
      fg_alpha=0.8,
      x=local_conf.gpu_temp_x_loc, y=local_conf.gpu_temp_y_loc,
      radius=local_conf.gpu_radius,
      thickness=local_conf.gpu_thickness,
      start_angle=-180,
      end_angle=180
    },
  }
  for i=1,num_cpu,1
  do
    settings_table[#settings_table+1] = {
      name='cpu',
      arg='cpu' .. tostring(i),
      max=100,
      bg_colour=0xffffff,
      bg_alpha=0.1,
      fg_colour=0xffffff,
      fg_alpha=0.7,
      x=local_conf.cpu_x_loc, y=local_conf.cpu_y_loc+begin_radius+(cpu_ring_count*cpu_ring_thickness)+1,
      radius=cur_radius,
      thickness=cpu_ring_thickness,
      start_angle=begin_angle,
      end_angle=begin_angle+(360/sections)
    }
    cur_radius=cur_radius+6
    if i%cpu_ring_count == 0 then begin_angle=begin_angle+360/sections cur_radius=begin_radius end
  end
  return settings_table
end