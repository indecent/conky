clock_x_loc = 180
clock_y_loc = 180

function local_conf ()
  local_conf = {
    clock_x_loc = clock_x_loc,
    clock_y_loc = clock_y_loc,
    clock_cpu_inner_arc_x_loc = clock_x_loc,
    clock_cpu_inner_arc_radius = 100,
    clock_cpu_outer_arc_x_loc = clock_x_loc,
    clock_cpu_outer_arc_radius = 625,
    clock_r = 65,
    clock_sec_thickness = 4,
    clock_min_thickness = 6,
    clock_hour_thickness = 8,
    cpu_x_loc = clock_x_loc,
    cpu_y_loc = clock_y_loc,
    cpu_ring_count = 4,
    cpu_ring_thickness = 5,
    cpu_begin_radius = 50,
    num_cpu = 12,
    mem_x_loc = clock_x_loc,
    mem_y_loc = clock_y_loc,
    mem_radius = 70,
    mem_thickness = 20,
    fs_x_loc = clock_x_loc,
    fs_y_loc = clock_y_loc,
    fs_radius = 50,
    fs_thickness = 4,
    fs_space = 40,
    gpu_temp_x_loc = 140,
    gpu_temp_y_loc = 510,
    gpu_fan_x_loc = 140,
    gpu_fan_y_loc = 510,
    gpu_radius = 65,
    gpu_thickness = 10,
    interface = 'enp4s0',
    xinerama_head = 2,
  }
  return local_conf
end